function montarSlick() {
    $('.banner').slick({
        arrows: true,
        dots: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
}

$(document).ready(function() {
    montarSlick();
});