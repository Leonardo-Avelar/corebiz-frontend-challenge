var formData = {
    "name": '',
    "email": '',
    "notice": '',
    "phone": ''
}

function setValues() {
    formData.name = $('.name').val();
    formData.email = $('.email').val();
    formData.notice = $('.notice').val();
    formData.phone = $('.phone').val();
}

function saveData() {
    console.log('data');
    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json'
        },
        // url: 'https://api.vtexcrm.com.br/powerlook/dataentities/FR/documents/',
        url: 'http://api.vtexcrm.com.br/corebiz/dataentities/TE/documents/',
        type: 'PATCH',
        data: JSON.stringify(formData),
        success: function (data) {
            if ( data.Id ) {
                fadeOut_form();
                $(components.form.selector).append('<h3 class="x-franqueado__status x-franqueado__status--success">Seus dados foram salvos com sucesso!</h3>');
                scrollUp_form();
            } else {
                fadeOut_form();
                $(components.form.selector).append('<h3 class="x-franqueado__status x-franqueado__status--error">Não foi possível salvar as informações, por favor, tente novamente.</h3>');
                scrollUp_form();
            }
        },
        fail: function (data) {
        }
    });
}