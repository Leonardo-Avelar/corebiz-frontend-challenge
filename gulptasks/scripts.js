import connect    from 'gulp-connect';
import sourcemaps from 'gulp-sourcemaps';
import source     from 'vinyl-source-stream';
import buffer     from 'vinyl-buffer';
import browserify from 'browserify';
import watchify   from 'watchify';
import babel      from 'babelify';
import gulpif     from 'gulp-if';
import uglify     from 'gulp-uglify';

module.exports = (gulp, dirs, argv) => {

    const scriptsPaths = {
        src: `${dirs.src}/assets/scripts/index.js`,
        dest: `${dirs.dest}/assets/scripts/`
    };

    let condition = (argv) => {
        if (argv == '--dev') {
            return true;
        }
    };

    gulp.task('scripts', () => {
        return compile();
    });

    function compile(watch) {
        
        var bundler = watchify(browserify(scriptsPaths.src, { debug: true }).transform(babel, {presets: ["@babel/preset-env"]}));
    
        function rebundle() {
            bundler.bundle()
                .on('error', function(err) { console.error(err); this.emit('end'); })
                .pipe(source('build.js'))
                .pipe(buffer())
                .pipe(gulpif( condition(argv), sourcemaps.init({ loadMaps: true }) ))
                .pipe(gulpif( condition(argv), sourcemaps.write('./') ))
                .pipe(gulpif( !condition(argv), uglify() ))
                .pipe(gulp.dest(scriptsPaths.dest))
                .pipe(connect.reload());
        }
    
        if (watch) {
            bundler.on('update', function() {
                var date = new Date();
                var hours, minutes, seconds;
                hours   = date.getHours();
                minutes = date.getMinutes();
                seconds = date.getSeconds();

                console.log("["+ hours +":"+ minutes +":"+ seconds +"] bundling...");
                rebundle();
            });
        }
    
            rebundle();
    }
    
    function watch() {
        return compile(true);
    };

    // gulp.task('build', function() { return compile(); });
    // gulp.task('watch', function() { return watch(); });
}