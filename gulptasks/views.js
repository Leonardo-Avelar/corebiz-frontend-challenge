import connect from 'gulp-connect';
import pug     from 'gulp-pug';
import gulpif  from 'gulp-if';

module.exports = (gulp, dirs, argv) => {

    const viewsPaths = {
        src: `${dirs.src}/views/*.pug`,
        dest: `${dirs.dest}/`
    };

    let condition = (argv) => {
        if (argv == '--dev') {
            return true;
        }
    };

    gulp.task('views', () => {

        return gulp.src(viewsPaths.src)
            .pipe(gulpif(condition(argv), pug({ pretty: true }), pug()))
            .pipe(gulp.dest(viewsPaths.dest))
            .pipe(connect.reload())
    });

    /**
     * PUG Watch
     */
    gulp.task('watchViews', () => {

        return gulp.watch(`${dirs.src}/views/**/*.pug`, ['views'])        
            .on('change', (event) => {
                let colorRed   = '\u001b[31m';
                let colorBlue  = '\u001b[34m';
                console.log(`${colorBlue} File ${colorRed}${event.path}${colorBlue} was ${event.type}, running tasks...`)
            })
    });
}