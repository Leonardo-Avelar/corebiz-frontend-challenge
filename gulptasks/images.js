import cache    from 'gulp-cache';
import imagemin from 'gulp-imagemin';

module.exports = (gulp, dirs) => {

    const imagesPaths = {
        src: `${dirs.src}/assets/images/*.+(png|jpg|gif|svg)`,
        dest: `${dirs.dest}/assets/images/`
    };

    gulp.task('images', () => {
        
        return gulp.src(imagesPaths.src)
            .pipe(cache(
                imagemin({
                    interlaced: true,
                    progressive: true,
                    optimizationLevel: 5,
                    svgoPlugins: [
                        {
                            removeViewBox: true
                        }
                    ]
                })
            ))
            .pipe(gulp.dest(imagesPaths.dest))
    });
}