import spritesmith from 'gulp.spritesmith';
import merge       from 'merge-stream';

module.exports = (gulp, dirs) => {

    const spritePaths = {
        src: `${dirs.src}/assets/images/sprite/*.+(png|jpg|gif|svg)`,
        destImg: `${dirs.src}/assets/images/`,
        destScss: `${dirs.src}/assets/styles/utils/`
    };

    gulp.task('sprites', () => {
        
        let spriteData = gulp.src(spritePaths.src).pipe(spritesmith({
            imgName: '../images/sprite.png',
            cssName: '_sprite.scss'
        }));

        let imgStream = spriteData.img
            .pipe(gulp.dest(spritePaths.destImg));

        let cssStream = spriteData.css
            .pipe(gulp.dest(spritePaths.destScss));

        return merge(imgStream, cssStream);
    });

}