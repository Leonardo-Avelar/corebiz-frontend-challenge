import connect      from 'gulp-connect';
import sass         from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps   from 'gulp-sourcemaps';
import gulpif       from 'gulp-if';

module.exports = (gulp, dirs, argv) => {

    const stylesPaths = {
        src: `${dirs.src}/assets/styles/*.scss`,
        dest: `${dirs.dest}/assets/styles/`
    }

    let condition = (argv) => {
        if (argv == '--dev') {
            return true;
        }
    };

    gulp.task('styles', () => {

        return gulp.src(stylesPaths.src)
            .pipe( gulpif( condition(argv), sourcemaps.init()))
            .pipe(
                gulpif( condition(argv), 
                    sass.sync().on('error', sass.logError),
                    sass.sync({ outputStyle: 'compressed' }).on('error', sass.logError)
                )
            )
            .pipe( autoprefixer())
            .pipe( gulpif( condition(argv), sourcemaps.write('.')))
            .pipe(gulp.dest(stylesPaths.dest))
            .pipe(connect.reload());
    });

    gulp.task('stylesdfsa', () => {

        switch (argv) {

            case '--dev':
                return gulp.src(stylesPaths.src)
                    .pipe(sourcemaps.init())
                    .pipe(sass.sync().on('error', sass.logError))
                    .pipe(autoprefixer())
                    .pipe(sourcemaps.write('.'))
                    .pipe(gulp.dest(stylesPaths.dest))
                    .pipe(connect.reload());
                break;
            
            case '--prod':
                return gulp.src(stylesPaths.src)
                    .pipe(sass.sync({ outputStyle: 'compressed' }).on('error', sass.logError))
                    .pipe(autoprefixer())
                    .pipe(gulp.dest(stylesPaths.dest))
                    .pipe(connect.reload());
                break;
        }
    });

    /**
     * SASS Watch
     */
    gulp.task('watchStyles', () => {

        return gulp.watch(`${dirs.src}/assets/styles/**/*.scss`, ['styles'])        
            .on('change', (event) => {
                let colorRed   = '\u001b[31m';
                let colorBlue  = '\u001b[34m';
                console.log(`${colorBlue} File ${colorRed}${event.path}${colorBlue} was ${event.type}, running tasks...`)
            })
    });
}