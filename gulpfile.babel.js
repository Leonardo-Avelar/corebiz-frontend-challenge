/**
 * Dependencies
 */
import gulp    from 'gulp';
import connect from 'gulp-connect';
import clean   from 'gulp-clean';
import run     from 'run-sequence';
import glob    from 'glob';

let dest;
let argv = process.argv[2];

switch (argv) {
    
    case '--dev':
        dest = 'dev';
        break;

    case '--prod':
        dest = 'prod';
        break;
}

/**
 * Directories
 */
const dirs = {
    src: 'src',
    dest: dest
};

/**
 * Server
 */
gulp.task('connect', () => {
    connect.server({
        root: dirs.dest,
        livereload: true
    })
});

/**
 * Load Modules
 */
glob.sync('./gulptasks/*.js', {}).forEach( (file) => {
    require(file)(gulp, dirs, argv)
})

/**
 * 
 * Limpa diretórios de produção
 */
gulp.task('clean', () => gulp.src(dest).pipe(clean()))

gulp.task('default', () => run('clean', 'sprites', ['views', 'styles', 'scripts', 'images', 'connect', 'watch']))

// gulp.task('watch', () => run('watchViews', 'watchStyles', 'watchScripts', 'connect'))
gulp.task('watch', () => run('watchStyles', 'watchViews'))